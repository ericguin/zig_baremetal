// These two are the default empty implementations for exception handlers
export fn blockingHandler() void {
    while (true) {}
}

export fn nullHandler() void {}

// This comes from the linker script and represents the initial stack pointer address.
// Not a function, but pretend it is to suppress type error
extern fn _stack() void;

// These are the exception handlers, which are weakly linked to the default handlers
// in the linker script
extern fn resetHandler() void;
extern fn nmiHandler() void;
extern fn hardFaultHandler() void;
extern fn memoryManagementFaultHandler() void;
extern fn busFaultHandler() void;
extern fn usageFaultHandler() void;
extern fn svCallHandler() void;
extern fn debugMonitorHandler() void;
extern fn pendSVHandler() void;
extern fn sysTickHandler() void;

// External interrupts
extern fn WWDG_IRQHandler() void;
extern fn PVD_IRQHandler() void;
extern fn TAMP_STAMP_IRQHandler() void;
extern fn RTC_WKUP_IRQHandler() void;
extern fn FLASH_IRQHandler() void;
extern fn RCC_IRQHandler() void;
extern fn EXTI0_IRQHandler() void;
extern fn EXTI1_IRQHandler() void;
extern fn EXTI2_IRQHandler() void;
extern fn EXTI3_IRQHandler() void;
extern fn EXTI4_IRQHandler() void;
extern fn DMA1_Stream0_IRQHandler() void;
extern fn DMA1_Stream1_IRQHandler() void;
extern fn DMA1_Stream2_IRQHandler() void;
extern fn DMA1_Stream3_IRQHandler() void;
extern fn DMA1_Stream4_IRQHandler() void;
extern fn DMA1_Stream5_IRQHandler() void;
extern fn DMA1_Stream6_IRQHandler() void;
extern fn ADC_IRQHandler() void;
extern fn CAN1_TX_IRQHandler() void;
extern fn CAN1_RX0_IRQHandler() void;
extern fn CAN1_RX1_IRQHandler() void;
extern fn CAN1_SCE_IRQHandler() void;
extern fn EXTI9_5_IRQHandler() void;
extern fn TIM1_BRK_TIM9_IRQHandler() void;
extern fn TIM1_UP_TIM10_IRQHandler() void;
extern fn TIM1_TRG_COM_TIM11_IRQHandler() void;
extern fn TIM1_CC_IRQHandler() void;
extern fn TIM2_IRQHandler() void;
extern fn TIM3_IRQHandler() void;
extern fn TIM4_IRQHandler() void;
extern fn I2C1_EV_IRQHandler() void;
extern fn I2C1_ER_IRQHandler() void;
extern fn I2C2_EV_IRQHandler() void;
extern fn I2C2_ER_IRQHandler() void;
extern fn SPI1_IRQHandler() void;
extern fn SPI2_IRQHandler() void;
extern fn USART1_IRQHandler() void;
extern fn USART2_IRQHandler() void;
extern fn USART3_IRQHandler() void;
extern fn EXTI15_10_IRQHandler() void;
extern fn RTC_Alarm_IRQHandler() void;
extern fn OTG_FS_WKUP_IRQHandler() void;
extern fn TIM8_BRK_TIM12_IRQHandler() void;
extern fn TIM8_UP_TIM13_IRQHandler() void;
extern fn TIM8_TRG_COM_TIM14_IRQHandler() void;
extern fn TIM8_CC_IRQHandler() void;
extern fn DMA1_Stream7_IRQHandler() void;
extern fn FMC_IRQHandler() void;
extern fn SDMMC1_IRQHandler() void;
extern fn TIM5_IRQHandler() void;
extern fn SPI3_IRQHandler() void;
extern fn UART4_IRQHandler() void;
extern fn UART5_IRQHandler() void;
extern fn TIM6_DAC_IRQHandler() void;
extern fn TIM7_IRQHandler() void;
extern fn DMA2_Stream0_IRQHandler() void;
extern fn DMA2_Stream1_IRQHandler() void;
extern fn DMA2_Stream2_IRQHandler() void;
extern fn DMA2_Stream3_IRQHandler() void;
extern fn DMA2_Stream4_IRQHandler() void;
extern fn ETH_IRQHandler() void;
extern fn ETH_WKUP_IRQHandler() void;
extern fn CAN2_TX_IRQHandler() void;
extern fn CAN2_RX0_IRQHandler() void;
extern fn CAN2_RX1_IRQHandler() void;
extern fn CAN2_SCE_IRQHandler() void;
extern fn OTG_FS_IRQHandler() void;
extern fn DMA2_Stream5_IRQHandler() void;
extern fn DMA2_Stream6_IRQHandler() void;
extern fn DMA2_Stream7_IRQHandler() void;
extern fn USART6_IRQHandler() void;
extern fn I2C3_EV_IRQHandler() void;
extern fn I2C3_ER_IRQHandler() void;
extern fn OTG_HS_EP1_OUT_IRQHandler() void;
extern fn OTG_HS_EP1_IN_IRQHandler() void;
extern fn OTG_HS_WKUP_IRQHandler() void;
extern fn OTG_HS_IRQHandler() void;
extern fn DCMI_IRQHandler() void;
extern fn RNG_IRQHandler() void;
extern fn FPU_IRQHandler() void;
extern fn UART7_IRQHandler() void;
extern fn UART8_IRQHandler() void;
extern fn SPI4_IRQHandler() void;
extern fn SPI5_IRQHandler() void;
extern fn SPI6_IRQHandler() void;
extern fn SAI1_IRQHandler() void;
extern fn LTDC_IRQHandler() void;
extern fn LTDC_ER_IRQHandler() void;
extern fn DMA2D_IRQHandler() void;
extern fn SAI2_IRQHandler() void;
extern fn QUADSPI_IRQHandler() void;
extern fn LPTIM1_IRQHandler() void;
extern fn CEC_IRQHandler() void;
extern fn I2C4_EV_IRQHandler() void;
extern fn I2C4_ER_IRQHandler() void;
extern fn SPDIF_RX_IRQHandler() void;
extern fn DFSDM1_FLT0_IRQHandler() void;
extern fn DFSDM1_FLT1_IRQHandler() void;
extern fn DFSDM1_FLT2_IRQHandler() void;
extern fn DFSDM1_FLT3_IRQHandler() void;
extern fn SDMMC2_IRQHandler() void;
extern fn CAN3_TX_IRQHandler() void;
extern fn CAN3_RX0_IRQHandler() void;
extern fn CAN3_RX1_IRQHandler() void;
extern fn CAN3_SCE_IRQHandler() void;
extern fn JPEG_IRQHandler() void;
extern fn MDIOS_IRQHandler() void;

// The vector table
export const vector_table linksection(".vectabl") = [126]?*const fn () callconv(.C) void{
    _stack,
    resetHandler, // Reset
    nmiHandler, // NMI
    hardFaultHandler, // Hard fault
    memoryManagementFaultHandler, // Memory management fault
    busFaultHandler, // Bus fault
    usageFaultHandler, // Usage fault
    null, // Reserved 1
    null, // Reserved 2
    null, // Reserved 3
    null, // Reserved 4
    svCallHandler, // SVCall
    debugMonitorHandler, // Debug monitor
    null, // Reserved 5
    pendSVHandler, // PendSV
    sysTickHandler, // SysTick
    WWDG_IRQHandler, //* Window WatchDog              */
    PVD_IRQHandler, //* PVD through EXTI Line detection */
    TAMP_STAMP_IRQHandler, //* Tamper and TimeStamps through the EXTI line */
    RTC_WKUP_IRQHandler, //* RTC Wakeup through the EXTI line */
    FLASH_IRQHandler, //* FLASH                        */
    RCC_IRQHandler, //* RCC                          */
    EXTI0_IRQHandler, //* EXTI Line0                   */
    EXTI1_IRQHandler, //* EXTI Line1                   */
    EXTI2_IRQHandler, //* EXTI Line2                   */
    EXTI3_IRQHandler, //* EXTI Line3                   */
    EXTI4_IRQHandler, //* EXTI Line4                   */
    DMA1_Stream0_IRQHandler, //* DMA1 Stream 0                */
    DMA1_Stream1_IRQHandler, //* DMA1 Stream 1                */
    DMA1_Stream2_IRQHandler, //* DMA1 Stream 2                */
    DMA1_Stream3_IRQHandler, //* DMA1 Stream 3                */
    DMA1_Stream4_IRQHandler, //* DMA1 Stream 4                */
    DMA1_Stream5_IRQHandler, //* DMA1 Stream 5                */
    DMA1_Stream6_IRQHandler, //* DMA1 Stream 6                */
    ADC_IRQHandler, //* ADC1, ADC2 and ADC3s         */
    CAN1_TX_IRQHandler, //* CAN1 TX                      */
    CAN1_RX0_IRQHandler, //* CAN1 RX0                     */
    CAN1_RX1_IRQHandler, //* CAN1 RX1                     */
    CAN1_SCE_IRQHandler, //* CAN1 SCE                     */
    EXTI9_5_IRQHandler, //* External Line[9:5]s          */
    TIM1_BRK_TIM9_IRQHandler, //* TIM1 Break and TIM9          */
    TIM1_UP_TIM10_IRQHandler, //* TIM1 Update and TIM10        */
    TIM1_TRG_COM_TIM11_IRQHandler, //* TIM1 Trigger and Commutation and TIM11 */
    TIM1_CC_IRQHandler, //* TIM1 Capture Compare         */
    TIM2_IRQHandler, //* TIM2                         */
    TIM3_IRQHandler, //* TIM3                         */
    TIM4_IRQHandler, //* TIM4                         */
    I2C1_EV_IRQHandler, //* I2C1 Event                   */
    I2C1_ER_IRQHandler, //* I2C1 Error                   */
    I2C2_EV_IRQHandler, //* I2C2 Event                   */
    I2C2_ER_IRQHandler, //* I2C2 Error                   */
    SPI1_IRQHandler, //* SPI1                         */
    SPI2_IRQHandler, //* SPI2                         */
    USART1_IRQHandler, //* USART1                       */
    USART2_IRQHandler, //* USART2                       */
    USART3_IRQHandler, //* USART3                       */
    EXTI15_10_IRQHandler, //* External Line[15:10]s        */
    RTC_Alarm_IRQHandler, //* RTC Alarm (A and B) through EXTI Line */
    OTG_FS_WKUP_IRQHandler, //* USB OTG FS Wakeup through EXTI line */
    TIM8_BRK_TIM12_IRQHandler, //* TIM8 Break and TIM12         */
    TIM8_UP_TIM13_IRQHandler, //* TIM8 Update and TIM13        */
    TIM8_TRG_COM_TIM14_IRQHandler, //* TIM8 Trigger and Commutation and TIM14 */
    TIM8_CC_IRQHandler, //* TIM8 Capture Compare         */
    DMA1_Stream7_IRQHandler, //* DMA1 Stream7                 */
    FMC_IRQHandler, //* FMC                          */
    SDMMC1_IRQHandler, //* SDMMC1                       */
    TIM5_IRQHandler, //* TIM5                         */
    SPI3_IRQHandler, //* SPI3                         */
    UART4_IRQHandler, //* UART4                        */
    UART5_IRQHandler, //* UART5                        */
    TIM6_DAC_IRQHandler, //* TIM6 and DAC1&2 underrun errors */
    TIM7_IRQHandler, //* TIM7                         */
    DMA2_Stream0_IRQHandler, //* DMA2 Stream 0                */
    DMA2_Stream1_IRQHandler, //* DMA2 Stream 1                */
    DMA2_Stream2_IRQHandler, //* DMA2 Stream 2                */
    DMA2_Stream3_IRQHandler, //* DMA2 Stream 3                */
    DMA2_Stream4_IRQHandler, //* DMA2 Stream 4                */
    ETH_IRQHandler, //* Ethernet                     */
    ETH_WKUP_IRQHandler, //* Ethernet Wakeup through EXTI line */
    CAN2_TX_IRQHandler, //* CAN2 TX                      */
    CAN2_RX0_IRQHandler, //* CAN2 RX0                     */
    CAN2_RX1_IRQHandler, //* CAN2 RX1                     */
    CAN2_SCE_IRQHandler, //* CAN2 SCE                     */
    OTG_FS_IRQHandler, //* USB OTG FS                   */
    DMA2_Stream5_IRQHandler, //* DMA2 Stream 5                */
    DMA2_Stream6_IRQHandler, //* DMA2 Stream 6                */
    DMA2_Stream7_IRQHandler, //* DMA2 Stream 7                */
    USART6_IRQHandler, //* USART6                       */
    I2C3_EV_IRQHandler, //* I2C3 event                   */
    I2C3_ER_IRQHandler, //* I2C3 error                   */
    OTG_HS_EP1_OUT_IRQHandler, //* USB OTG HS End Point 1 Out   */
    OTG_HS_EP1_IN_IRQHandler, //* USB OTG HS End Point 1 In    */
    OTG_HS_WKUP_IRQHandler, //* USB OTG HS Wakeup through EXTI */
    OTG_HS_IRQHandler, //* USB OTG HS                   */
    DCMI_IRQHandler, //* DCMI                         */
    null, //* Reserved                     */
    RNG_IRQHandler, //* RNG                          */
    FPU_IRQHandler, //* FPU                          */
    UART7_IRQHandler, //* UART7                        */
    UART8_IRQHandler, //* UART8                        */
    SPI4_IRQHandler, //* SPI4                         */
    SPI5_IRQHandler, //* SPI5                         */
    SPI6_IRQHandler, //* SPI6                         */
    SAI1_IRQHandler, //* SAI1                         */
    LTDC_IRQHandler, //* LTDC                         */
    LTDC_ER_IRQHandler, //* LTDC error                   */
    DMA2D_IRQHandler, //* DMA2D                        */
    SAI2_IRQHandler, //* SAI2                         */
    QUADSPI_IRQHandler, //* QUADSPI                      */
    LPTIM1_IRQHandler, //* LPTIM1                       */
    CEC_IRQHandler, //* HDMI_CEC                     */
    I2C4_EV_IRQHandler, //* I2C4 Event                   */
    I2C4_ER_IRQHandler, //* I2C4 Error                   */
    SPDIF_RX_IRQHandler, //* SPDIF_RX                     */
    null, //* Reserved                     */
    DFSDM1_FLT0_IRQHandler, //* DFSDM1 Filter 0 global Interrupt */
    DFSDM1_FLT1_IRQHandler, //* DFSDM1 Filter 1 global Interrupt */
    DFSDM1_FLT2_IRQHandler, //* DFSDM1 Filter 2 global Interrupt */
    DFSDM1_FLT3_IRQHandler, //* DFSDM1 Filter 3 global Interrupt */
    SDMMC2_IRQHandler, //* SDMMC2                       */
    CAN3_TX_IRQHandler, //* CAN3 TX                      */
    CAN3_RX0_IRQHandler, //* CAN3 RX0                     */
    CAN3_RX1_IRQHandler, //* CAN3 RX1                     */
    CAN3_SCE_IRQHandler, //* CAN3 SCE                     */
    JPEG_IRQHandler, //* JPEG                         */
    MDIOS_IRQHandler, //* MDIOS                        */
};
