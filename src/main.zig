const std = @import("std");
const math = std.math;

pub fn RORegister(comptime Reg: type) type {
    return struct {
        raw_ptr: *volatile u32,

        const Self = @This();

        pub fn init(address: usize) Self {
            return Self{ .raw_ptr = @ptrFromInt(address) };
        }

        pub fn read(self: Self) Reg {
            return @bitCast(self.raw_ptr.*);
        }
    };
}

pub fn WORegister(comptime Reg: type) type {
    return struct {
        raw_ptr: *volatile u32,

        const Self = @This();

        pub fn init(address: usize) Self {
            return Self{ .raw_ptr = @ptrFromInt(address) };
        }

        pub fn write(self: Self, data: Reg) void {
            self.raw_ptr.* = @bitCast(data);
        }
    };
}

pub fn CalculateRawPointerSize(comptime Reg: type) type {
    if (@sizeOf(Reg) == 4) {
        return *volatile u32;
    } else if (@sizeOf(Reg) == 2) {
        return *volatile u16;
    }

    @compileError("Invalid size of register descriptor");
}

pub fn RWRegister(comptime Reg: type) type {
    return struct {
        raw_ptr: CalculateRawPointerSize(Reg),

        const Self = @This();

        pub fn init(address: usize) Self {
            return Self{ .raw_ptr = @ptrFromInt(address) };
        }

        pub fn read(self: Self) Reg {
            return @bitCast(self.raw_ptr.*);
        }

        pub fn write(self: Self, data: Reg) void {
            self.raw_ptr.* = @bitCast(data);
        }

        pub fn modify(self: Self, data: anytype) void {
            var old = self.read();

            const info = @typeInfo(@TypeOf(data));

            inline for (info.Struct.fields) |field| {
                @field(old, field.name) = @field(data, field.name);
            }

            self.write(old);
        }
    };
}

pub const GPIOB = struct {
    const base_address = 0x4002_0400;

    const MODER_val = enum(u2) {
        Input = 0b00,
        GeneralPurposeOutput = 0b01,
        AlternateFunction = 0b10,
        Analog = 0b11,
    };

    const MODER_reg = packed struct {
        MODER0: MODER_val = .Input,
        MODER1: MODER_val = .Input,
        MODER2: MODER_val = .Input,
        MODER3: MODER_val = .Input,
        MODER4: MODER_val = .Input,
        MODER5: MODER_val = .Input,
        MODER6: MODER_val = .Input,
        MODER7: MODER_val = .Input,
        MODER8: MODER_val = .Input,
        MODER9: MODER_val = .Input,
        MODER10: MODER_val = .Input,
        MODER11: MODER_val = .Input,
        MODER12: MODER_val = .Input,
        MODER13: MODER_val = .Input,
        MODER14: MODER_val = .Input,
        MODER15: MODER_val = .Input,
    };

    pub const MODER = RWRegister(MODER_reg).init(base_address + 0x0);

    const OTYPER_val = enum(u1) {
        PushPull = 0b0,
        OpenDrain = 0b1,
    };

    const OTYPER_reg = packed struct {
        OT0: OTYPER_val = .PushPull,
        OT1: OTYPER_val = .PushPull,
        OT2: OTYPER_val = .PushPull,
        OT3: OTYPER_val = .PushPull,
        OT4: OTYPER_val = .PushPull,
        OT5: OTYPER_val = .PushPull,
        OT6: OTYPER_val = .PushPull,
        OT7: OTYPER_val = .PushPull,
        OT8: OTYPER_val = .PushPull,
        OT9: OTYPER_val = .PushPull,
        OT10: OTYPER_val = .PushPull,
        OT11: OTYPER_val = .PushPull,
        OT12: OTYPER_val = .PushPull,
        OT13: OTYPER_val = .PushPull,
        OT14: OTYPER_val = .PushPull,
        OT15: OTYPER_val = .PushPull,
        _reserved: u16 = 0,
    };

    pub const OTYPER = RWRegister(OTYPER_reg).init(base_address + 0x4);

    const OSPEEDR_val = enum(u2) {
        LowSpeed = 0b00,
        MediumSpeed = 0b01,
        HighSpeed = 0b10,
        VeryhighSpeed = 0b11,
    };

    const OSPEEDR_reg = packed struct {
        OSPEEDR0: OSPEEDR_val = .LowSpeed,
        OSPEEDR1: OSPEEDR_val = .LowSpeed,
        OSPEEDR2: OSPEEDR_val = .LowSpeed,
        OSPEEDR3: OSPEEDR_val = .LowSpeed,
        OSPEEDR4: OSPEEDR_val = .LowSpeed,
        OSPEEDR5: OSPEEDR_val = .LowSpeed,
        OSPEEDR6: OSPEEDR_val = .LowSpeed,
        OSPEEDR7: OSPEEDR_val = .LowSpeed,
        OSPEEDR8: OSPEEDR_val = .LowSpeed,
        OSPEEDR9: OSPEEDR_val = .LowSpeed,
        OSPEEDR10: OSPEEDR_val = .LowSpeed,
        OSPEEDR11: OSPEEDR_val = .LowSpeed,
        OSPEEDR12: OSPEEDR_val = .LowSpeed,
        OSPEEDR13: OSPEEDR_val = .LowSpeed,
        OSPEEDR14: OSPEEDR_val = .LowSpeed,
        OSPEEDR15: OSPEEDR_val = .LowSpeed,
    };

    pub const OSPEEDR = RWRegister(OSPEEDR_reg).init(base_address + 0x08);

    const PUPDR_val = enum(u2) {
        None = 0b00,
        PullUp = 0b01,
        PullDown = 0b10,
    };

    const PUPDR_reg = packed struct {
        PUPDR0: PUPDR_val = .None,
        PUPDR1: PUPDR_val = .None,
        PUPDR2: PUPDR_val = .None,
        PUPDR3: PUPDR_val = .None,
        PUPDR4: PUPDR_val = .None,
        PUPDR5: PUPDR_val = .None,
        PUPDR6: PUPDR_val = .None,
        PUPDR7: PUPDR_val = .None,
        PUPDR8: PUPDR_val = .None,
        PUPDR9: PUPDR_val = .None,
        PUPDR10: PUPDR_val = .None,
        PUPDR11: PUPDR_val = .None,
        PUPDR12: PUPDR_val = .None,
        PUPDR13: PUPDR_val = .None,
        PUPDR14: PUPDR_val = .None,
        PUPDR15: PUPDR_val = .None,
    };

    pub const PUPDR = RWRegister(PUPDR_reg).init(base_address + 0x0C);

    const IODR_val = enum(u1) {
        Low = 0b0,
        High = 0b1,
    };

    const IDR_reg = packed struct {
        IDR0: IODR_val = .Low,
        IDR1: IODR_val = .Low,
        IDR2: IODR_val = .Low,
        IDR3: IODR_val = .Low,
        IDR4: IODR_val = .Low,
        IDR5: IODR_val = .Low,
        IDR6: IODR_val = .Low,
        IDR7: IODR_val = .Low,
        IDR8: IODR_val = .Low,
        IDR9: IODR_val = .Low,
        IDR10: IODR_val = .Low,
        IDR11: IODR_val = .Low,
        IDR12: IODR_val = .Low,
        IDR13: IODR_val = .Low,
        IDR14: IODR_val = .Low,
        IDR15: IODR_val = .Low,
        _reserved: u16 = 0,
    };

    const ODR_reg = packed struct {
        ODR0: IODR_val = .Low,
        ODR1: IODR_val = .Low,
        ODR2: IODR_val = .Low,
        ODR3: IODR_val = .Low,
        ODR4: IODR_val = .Low,
        ODR5: IODR_val = .Low,
        ODR6: IODR_val = .Low,
        ODR7: IODR_val = .Low,
        ODR8: IODR_val = .Low,
        ODR9: IODR_val = .Low,
        ODR10: IODR_val = .Low,
        ODR11: IODR_val = .Low,
        ODR12: IODR_val = .Low,
        ODR13: IODR_val = .Low,
        ODR14: IODR_val = .Low,
        ODR15: IODR_val = .Low,
        _reserved: u16 = 0,
    };

    pub const IDR = RORegister(IDR_reg).init(base_address + 0x10);
    pub const ODR = RWRegister(ODR_reg).init(base_address + 0x14);

    const BS_val = enum(u1) {
        DontSet = 0b0,
        Set = 0b1,
    };

    const BR_val = enum(u1) {
        DontClear = 0b0,
        Clear = 0b1,
    };

    const BSRR_reg = packed struct {
        BS0: BS_val = .DontSet,
        BS1: BS_val = .DontSet,
        BS2: BS_val = .DontSet,
        BS3: BS_val = .DontSet,
        BS4: BS_val = .DontSet,
        BS5: BS_val = .DontSet,
        BS6: BS_val = .DontSet,
        BS7: BS_val = .DontSet,
        BS8: BS_val = .DontSet,
        BS9: BS_val = .DontSet,
        BS10: BS_val = .DontSet,
        BS11: BS_val = .DontSet,
        BS12: BS_val = .DontSet,
        BS13: BS_val = .DontSet,
        BS14: BS_val = .DontSet,
        BS15: BS_val = .DontSet,
        BR0: BR_val = .DontClear,
        BR1: BR_val = .DontClear,
        BR2: BR_val = .DontClear,
        BR3: BR_val = .DontClear,
        BR4: BR_val = .DontClear,
        BR5: BR_val = .DontClear,
        BR6: BR_val = .DontClear,
        BR7: BR_val = .DontClear,
        BR8: BR_val = .DontClear,
        BR9: BR_val = .DontClear,
        BR10: BR_val = .DontClear,
        BR11: BR_val = .DontClear,
        BR12: BR_val = .DontClear,
        BR13: BR_val = .DontClear,
        BR14: BR_val = .DontClear,
        BR15: BR_val = .DontClear,
    };

    pub const BSRR = WORegister(BSRR_reg).init(base_address + 0x18);

    // TODO: Lock register
    // TODO: alt function high/low
};

pub const RCC = struct {
    const base_address: u32 = 0x4002_3800;

    const ENR_val = enum(u1) {
        Disabled = 0b0,
        Enabled = 0b1,
    };

    const AHB1ENR_reg = packed struct {
        GPIOA_EN: ENR_val = .Disabled,
        GPIOB_EN: ENR_val = .Disabled,
        GPIOC_EN: ENR_val = .Disabled,
        GPIOD_EN: ENR_val = .Disabled,
        GPIOE_EN: ENR_val = .Disabled,
        GPIOF_EN: ENR_val = .Disabled,
        GPIOG_EN: ENR_val = .Disabled,
        GPIOH_EN: ENR_val = .Disabled,
        GPIOI_EN: ENR_val = .Disabled,
        GPIOJ_EN: ENR_val = .Disabled,
        GPIOK_EN: ENR_val = .Disabled,
        _reserved1: u1 = 0,
        CRC_EN: ENR_val = .Disabled,
        _reserved2: u5 = 0,
        BKPSRAM_EN: ENR_val = .Disabled,
        _reserved3: u1 = 0,
        DTCMRAM_EN: ENR_val = .Disabled,
        DMA1_EN: ENR_val = .Disabled,
        DMA2_EN: ENR_val = .Disabled,
        DMA2D_EN: ENR_val = .Disabled,
        _reserved4: u1 = 0,
        ETHMAC_EN: ENR_val = .Disabled,
        ETHMACTX_EN: ENR_val = .Disabled,
        ETHMACRX_EN: ENR_val = .Disabled,
        ETHMACPTP_EN: ENR_val = .Disabled,
        OTGHS_EN: ENR_val = .Disabled,
        OTGHSULPI_EN: ENR_val = .Disabled,
        _reserved5: u1 = 0,
    };

    pub const AHB1ENR = RWRegister(AHB1ENR_reg).init(base_address + 0x30);

    const APB1ENR_reg = packed struct {
        TIM2_EN: ENR_val = .Disabled,
        TIM3_EN: ENR_val = .Disabled,
        TIM4_EN: ENR_val = .Disabled,
        TIM5_EN: ENR_val = .Disabled,
        TIM6_EN: ENR_val = .Disabled,
        TIM7_EN: ENR_val = .Disabled,
        TIM12_EN: ENR_val = .Disabled,
        TIM13_EN: ENR_val = .Disabled,
        TIM14_EN: ENR_val = .Disabled,
        LPTIM1_EN: ENR_val = .Disabled,
        RTCAPB_EN: ENR_val = .Enabled,
        WWDG_EN: ENR_val = .Disabled,
        _reserved1: u1 = 0,
        CAN3_EN: ENR_val = .Disabled,
        SPI2_EN: ENR_val = .Disabled,
        SPI3_EN: ENR_val = .Disabled,
        SPDIFRX_EN: ENR_val = .Disabled,
        USART2_EN: ENR_val = .Disabled,
        USART3_EN: ENR_val = .Disabled,
        UART4_EN: ENR_val = .Disabled,
        UART5_EN: ENR_val = .Disabled,
        I2C1_EN: ENR_val = .Disabled,
        I2C2_EN: ENR_val = .Disabled,
        I2C3_EN: ENR_val = .Disabled,
        I2C4_EN: ENR_val = .Disabled,
        CAN1_EN: ENR_val = .Disabled,
        CAN2_EN: ENR_val = .Disabled,
        CEC_EN: ENR_val = .Disabled,
        PWR_EN: ENR_val = .Disabled,
        DAC_EN: ENR_val = .Disabled,
        UART7_EN: ENR_val = .Disabled,
        UART8_EN: ENR_val = .Disabled,
    };

    pub const APB1ENR = RWRegister(APB1ENR_reg).init(base_address + 0x40);
};

pub const TIMX_Type = enum {
    SixteenBitTimer,
    ThirtyTwoBitTimer,
};

pub fn TIMX(comptime baseaddr: u32, comptime ttype: TIMX_Type) type {
    const timx_helpers = struct {
        pub fn generateCntType(comptime tt: TIMX_Type) type {
            if (tt == .SixteenBitTimer) {
                return packed struct {
                    CNT: u16 = 0,
                    _reserved: u15 = 0,
                    UIFCPY_RO: u1 = 0,
                };
            } else if (tt == .ThirtyTwoBitTimer) {
                return packed struct {
                    CNT: u31 = 0,
                    UIFCPY_RO: u1 = 0,
                };
            }
        }

        pub fn generateArrType(comptime tt: TIMX_Type) type {
            if (tt == .SixteenBitTimer) {
                return packed struct {
                    ARR: u16 = 0,
                    _reserved: u16 = 0,
                };
            } else if (tt == .ThirtyTwoBitTimer) {
                return packed struct {
                    ARR: u32 = 0,
                };
            }
        }
    };

    return struct {
        const base_address: u32 = baseaddr;

        const EN_DIS_val = enum(u1) {
            Disabled = 0,
            Enabled = 1,
        };

        const URS_val = enum(u1) {
            AnyEvent = 0,
            OnlyCounterOverUnder = 1,
        };

        const OPM_val = enum(u1) {
            DoesNotStop = 0,
            StopsOnUpdate = 1,
        };

        const DIR_val = enum(u1) {
            UpCounter = 0,
            DownCounter = 1,
        };

        const CMS_val = enum(u2) {
            EdgeAligned = 0b00,
            CenterAlignedDown = 0b01,
            CenterAlignedUp = 0b10,
            CenterAlignedEither = 0b11,
        };

        const APRE_val = enum(u1) {
            NotBuffered = 0,
            Buffered = 1,
        };

        const CKD_val = enum(u2) {
            Mult1x = 0b00,
            Mult2x = 0b01,
            Mult4x = 0b10,
        };

        const UIFREMAP_val = enum(u1) {
            NoRemapping = 0,
            Remapping = 1,
        };

        const CR1_reg = packed struct {
            CEN: EN_DIS_val = .Disabled,
            UDIS: EN_DIS_val = .Disabled,
            URS: URS_val = .AnyEvent,
            OPM: OPM_val = .DoesNotStop,
            DIR: DIR_val = .UpCounter,
            CMS: CMS_val = .EdgeAligned,
            APRE: APRE_val = .NotBuffered,
            CKD: CKD_val = .Mult1x,
            _reserved1: u1 = 0,
            UIFREMAP: UIFREMAP_val = .NoRemapping,
            _reserved2: u4 = 0,
        };

        pub const CR1 = RWRegister(CR1_reg).init(base_address + 0x00);

        const CCDS_val = enum(u1) {
            DMARequestOnCC = 0b0,
            DMARequestOnUpdate = 0b1,
        };

        const MMS_val = enum(u3) {
            Reset = 0b000,
            Enable = 0b001,
            Update = 0b010,
            ComparePulse = 0b011,
            CompareOC1Ref = 0b100,
            CompareOC2Ref = 0b101,
            CompareOC3Ref = 0b110,
            CompareOC4Ref = 0b111,
        };

        const TI1S_val = enum(u1) {
            CH1Only = 0b0,
            CH1XOR2XOR3 = 0b1,
        };

        const CR2_reg = packed struct {
            _reserved1: u3 = 0,
            CCDS: CCDS_val = .DMARequestOnCC,
            MMS: MMS_val = .Reset,
            TI1S: TI1S_val = .CH1Only,
            _reserved2: u8 = 0,
        };

        pub const CR2 = RWRegister(CR2_reg).init(base_address + 0x04);

        const DIER_reg = packed struct {
            UIE: EN_DIS_val = .Disabled,
            CC1IE: EN_DIS_val = .Disabled,
            CC2IE: EN_DIS_val = .Disabled,
            CC3IE: EN_DIS_val = .Disabled,
            CC4IE: EN_DIS_val = .Disabled,
            _reserved1: u1 = 0,
            TIE: EN_DIS_val = .Disabled,
            _reserved2: u1 = 0,
            UDE: EN_DIS_val = .Disabled,
            CC1DE: EN_DIS_val = .Disabled,
            CC2DE: EN_DIS_val = .Disabled,
            CC3DE: EN_DIS_val = .Disabled,
            CC4DE: EN_DIS_val = .Disabled,
            _reserved3: u1 = 0,
            TDE: EN_DIS_val = .Disabled,
            _reserved4: u1 = 0,
        };

        pub const DIER = RWRegister(DIER_reg).init(base_address + 0x0C);

        const EGR_reg = packed struct {
            UG: EN_DIS_val = .Disabled,
            CC1G: EN_DIS_val = .Disabled,
            CC2G: EN_DIS_val = .Disabled,
            CC3G: EN_DIS_val = .Disabled,
            CC4G: EN_DIS_val = .Disabled,
            _reserved1: u1 = 0,
            TG: EN_DIS_val = .Disabled,
            _reserved2: u9 = 0,
        };

        pub const EGR = RWRegister(EGR_reg).init(base_address + 0x14);

        const CNT_reg = timx_helpers.generateCntType(ttype);

        pub const CNT = RWRegister(CNT_reg).init(base_address + 0x24);

        const PSC_reg = packed struct {
            PSC: u16 = 0,
        };

        pub const PSC = RWRegister(PSC_reg).init(base_address + 0x28);

        const ARR_reg = timx_helpers.generateArrType(ttype);

        pub const ARR = RWRegister(ARR_reg).init(base_address + 0x2C);

        // TODO: the rest of them? IDK I should try to get this running soon.
    };
}

pub const TIM4 = TIMX(0x4000_0800, .SixteenBitTimer);

pub fn enable_interrupt(interrupt: u32) void {
    const reg = interrupt >> 5;
    const val = math.pow(u32, 2, interrupt - (reg * 32));
    const addr: *volatile u32 = @ptrFromInt(0xE000E100 + (reg * 4));
    addr.* = val;
}

pub fn clear_interrupt(interrupt: u32) void {
    const reg = interrupt >> 5;
    const val = math.pow(u32, 2, interrupt - (reg * 32));
    const addr: *volatile u32 = @ptrFromInt(0xE000E280 + (reg * 4));
    addr.* = val;
}

var count: i32 = 0;

pub fn handle_irq() void {
    // Read in the state
    count += 1;

    if (count >= 2000) {
        const current = GPIOB.ODR.read();

        if (current.ODR7 == .High) {
            GPIOB.BSRR.write(.{ .BR7 = .Clear });
        } else {
            GPIOB.BSRR.write(.{ .BS7 = .Set });
        }
        count = 0;
    }

    clear_interrupt(30);
}

export fn TIM4_IRQHandler() void {
    handle_irq();
}

pub fn main() void {
    const vtor: *volatile u32 = @ptrFromInt(0xE000ED08);

    const vtor_val = vtor.*;
    _ = vtor_val;

    // Enable clock
    RCC.AHB1ENR.modify(.{ .GPIOB_EN = .Enabled });
    RCC.APB1ENR.modify(.{ .TIM4_EN = .Enabled });

    var i: u32 = 0;
    while (i < 1000) {
        i += 1;
    }

    enable_interrupt(30);

    TIM4.DIER.modify(.{ .UIE = .Enabled });
    TIM4.PSC.write(.{ .PSC = 0xFFFF });
    TIM4.CR1.modify(.{ .CEN = .Enabled, .UIFREMAP = .Remapping });

    GPIOB.MODER.modify(.{ .MODER7 = .GeneralPurposeOutput });

    GPIOB.BSRR.write(.{ .BS7 = .Set });

    while (true) {
        // i = 0;

        // while (i < 500_000) {
        //     i += 1;
        // }

        // // Read in the state
        // const current = GPIOB.ODR.read();

        // if (current.ODR7 == .High) {
        //     GPIOB.BSRR.write(.{ .BR7 = .Clear });
        // } else {
        //     GPIOB.BSRR.write(.{ .BS7 = .Set });
        // }
    }
}
